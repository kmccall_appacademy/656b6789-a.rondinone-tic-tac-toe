class ComputerPlayer

  attr_reader :name
  attr_accessor :mark, :board

  def initialize(name)
    @name = name
    @mark = :O
  end

  def get_move
    available_moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        pos = row, col
        available_moves << pos if board.empty?(pos)
      end
    end

    available_moves.each do |move|
     return move if wins?(move)
   end

    available_moves.sample
  end

  def wins?(move)
    board.place_mark(move, @mark)
    if board.winner == :O
      true
    else
      board.grid[move[0]][move[1]] = nil
      false
    end
  end


  def display(board)
    @board = board
  end



end
