class HumanPlayer

  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = :X
  end

  def get_move
    puts "where do you want to move"
    move = gets.chomp
    move.delete(',').split.map(&:to_i)
  end

  def display(board)
    print board.grid
  end

end
