class Board

  attr_accessor :grid

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]] == nil
  end

  def place_mark(pos, mark)
    if empty?(pos)
      @grid[pos[0]][pos[1]] = mark
    end
  end

  def winner

    return check_row if check_row
    return check_column if check_column

    #Check Diaginals
    if @grid[0][0] == :X && @grid[1][1] == :X && @grid[2][2] == :X
      :X
    elsif @grid[0][2] == :X && @grid[1][1] == :X && @grid[2][0] == :X
      :X
    elsif @grid[0][0] == :O && @grid[1][1] == :O && @grid[2][2] == :O
      :O
    elsif @grid[0][2] == :O && @grid[1][1] == :O && @grid[2][0] == :O
      :O
    else
      nil
    end
  end

  def over?
    if !winner.nil?
      return true
    elsif @grid.flatten.all?{ |x| x != nil }
      return true
    end
    false
  end



  #  ====================================

  def check_row
    (0..2).each do |row|
      test = []
      (0..2).each do |column|
        test << @grid[row][column]
      end
      if test.all? {|x| x == :X}
        return test[0]
      elsif test.all? {|x| x == :O}
      return test[0]
      end
    end
    false
  end

  def check_column
    (0..2).each do |row|
      test = []
      (0..2).each do |column|
        test << @grid.transpose[row][column]
      end
      if test.all? {|x| x == :X}
        return test[0]
      elsif test.all? {|x| x == :O}
      return test[0]
      end
    end
    false
  end


end
